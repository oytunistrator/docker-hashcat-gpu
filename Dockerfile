FROM nvidia/cuda:12.1.0-base-ubuntu22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y build-essential git hashcat-nvidia hashcat bash

WORKDIR /upload

CMD ["hashcat"]

VOLUME ["/upload"]

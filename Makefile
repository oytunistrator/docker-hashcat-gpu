build:
	docker build . -t oytunistrator/hashcat-gpu:latest 

release:
	docker push oytunistrator/hashcat-gpu:latest

bash:
	docker run --runtime=nvidia --gpus all -it oytunistrator/hashcat-gpu:latest /bin/bash

compose:
	docker-compose up --build -d
	docker-compose exec system bash
	docker-compose down

all: build release



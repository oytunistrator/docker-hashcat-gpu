# Docker Hashcat for GPU

Run hashcat in docker with nvidia docker containers.

First install [nvidia-container](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) first.


Build from sources:

```
make build
```

After enter runtime:
```
make bash
```

Start shell:

```
docker run --runtime=nvidia --gpus all -it oytunistrator/hashcat-gpu:latest /bin/bash
```

Directly run hashcat:

```
docker run --runtime=nvidia --gpus all -it oytunistrator/hashcat-gpu:latest hashcat
```




